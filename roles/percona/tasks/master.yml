---
- name: UBUNTU16 - Ensure master.
  lineinfile:
    path: /etc/mysql/percona-server.conf.d/mysqld.cnf
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
  with_items:
    - { regexp: '^server_id', line: "server_id = 1"}
    - { regexp: '^log_bin ', line: "log_bin = /var/log/mysql/mysql-bin.log"}
    - { regexp: '^log_bin_index', line: "log_bin_index = /var/log/mysql/mysql-bin.log.index"}
    - { regexp: '^expire_logs_days', line: "expire_logs_days = 10"}
    - { regexp: '^max_binlog_size', line: "max_binlog_size = 100M"}
    - { regexp: '^binlog_ignore_db', line: "binlog_ignore_db = mysql"}
    - { regexp: '^bind_address', line: "bind_address = {{ hostvars[groups['perconamaster'][0]].ansible_default_ipv4.address }}"}
  notify:
    - reload_percona
  when: "ansible_distribution == 'Ubuntu' and ansible_distribution_major_version == '16'"
- name: CENTOS - Ensure master.
  ini_file:
    path: /etc/my.cnf
    section: mysqld
    option: "{{ item.option }}"
    value: "{{ item.value }}"
  with_items:
    - { option: server_id, value: 1 }
    - { option: log-bin, value: mysql-bin }
    - { option: bind_address, value: "{{ hostvars[groups['perconamaster'][0]].ansible_default_ipv4.address }}" }
  notify:
    - reload_percona
  when: "ansible_distribution == 'CentOS'"
- name: UBUNTU14 - Ensure master.
  ini_file:
    path: /etc/mysql/my.cnf
    section: mysqld
    option: "{{ item.option }}"
    value: "{{ item.value }}"
  with_items:
    - { option: server_id, value: 1}
    - { option: log_bin, value: /var/log/mysql/mysql-bin.log }
    - { option: log_bin_index, value: /var/log/mysql/mysql-bin.log.index" }
    - { option: expire_logs_days, value: 10 }
    - { option: max_binlog_size, value: 100M }
    - { option: binlog_ignore_db, value: mysql }
    - { option: bind_address, value: "{{ hostvars[groups['perconamaster'][0]].ansible_default_ipv4.address }}" }
  when: "ansible_distribution == 'Ubuntu' and ansible_distribution_major_version == '14'"

- name: CENTOS - Change mysql root user credentials.
  block:
  - name: CENTOS - Get temporary mysql root password
    command: >
      awk -F : '/temporary password/ {gsub(/ /, "", $NF); print $NF}' /var/log/mysqld.log
    register: temporary_password
  - set_fact:
      mysql_old_password: "{{ temporary_password.stdout }}"
  - name: CENTOS - Change password validation policy
    ini_file:
      path: /etc/my.cnf
      section: mysqld
      option: validate_password_policy
      value: LOW
    register: pw_policy
    notify:
      - reload_percona
  - meta: flush_handlers
  - name: CENTOS - Change temporary mysql root password
    command: mysql -u root --password={{ mysql_old_password }} --connect-expired-password -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('{{ mysql_root_pw }}');"
    when: pw_policy.changed
  when: "ansible_distribution == 'CentOS'"
  ignore_errors: yes

- name: ALL - Create mysql replication user
  mysql_user:
    name: "{{ mysql_replication_user | default('replica') }}"
    login_user: root
    login_password: "{{ mysql_root_pw | default('') }}"
    login_unix_socket: "{{ login_unix_socket }}"
    state: present
    host: "{{ mysql_replication_user_host | default('%') }}"
    check_implicit_admin: yes
    password: "{{ mysql_replication_password | default('replica') }}"
    priv: '*.*:REPLICATION SLAVE'
  notify:
    - reload_percona
- meta: flush_handlers

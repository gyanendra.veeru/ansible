Role Name
=========

Installs percona one master multi slave setup. 

Requirements
------------

python pymysql package is required. But however it is ensured in the role itself.

Role Variables
--------------

All parameters are defined in the defaults section.
For CENTOS, mysql root password = 'password' , for Ubuntu password = ''
For CENTOS, mysql replication user = 'replication', For Ubuntu replication user = 'replica'

Dependencies
------------

python pymysql package is required. But however it is ensured in the role itself. 

Example Playbook
----------------

Below is a sample playbook to use this role. In case you are not logging into nodes as root user. You will need to add privilege escalation to the playbook as well. 

---
- hosts: all
  roles:
   - percona

License
-------

BSD

Author Information
------------------

Gyanendra Veeru
LinkedIN: https://www.linkedin.com/in/gyanendraveeru/
